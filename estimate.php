<?php
// put all the files that are required at the top
require_once './classes/User.php'; //User class
require_once './classes/Job.php';  //Job class
require_once './classes/epk_db_connect.php'; // Class to connect to the database using mysqli
require_once './classes/utils.php'; // Utility class containting some housekeeping functions
require_once './classes/Quote.php'; //Quote Class
require_once './classes/Item.php';  //Item Class


// if there is nobody logged in, redirect to index.php
session_start();
$path_parts = pathinfo(__FILE__);
if(!isset($_SESSION['usr']) || !isset($_SESSION['time'])){
    header('location: login.php?redir=' . $path_parts['basename']);
    exit;
}

// specify the authorised users
$authrzd = array('owner', 'admin', 'techie'); // 'admin', 'cutter', 'customer', 'delivery', 'drafter', 'estimator', 'installer', 'painter', 'cabinate maker', 'painter', 'packer', 'owner''

// get the details of the logged in user
$usr = $_SESSION['usr'];
$user_type=$usr->getRole();


$d = new DB_CONNECT();
$cn = new mysqli();
$cn = $d->connect();
$stmt = "SELECT * FROM job WHERE order_id=".$_REQUEST['id'].";";
$result = $cn->query($stmt);

$proj = new Job();

if($result!=null){
    $result->data_seek(0);
    $row = $result->fetch_assoc();
    $proj->setValues($row);
}else{
    $proj=false;
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include_once 'includes/includehead.html'; ?>
        <title>Welcome to EPK Online</title>
        <style>
            .leftcol{
                width: 200px;
            }
            
            td{
                padding: 1px;
            }
            
            input{
                margin-left: 10px;
            }

            input[type="checkbox"]:checked + label {
                background: yellow;
            }

            input[type="radio"]:checked + label {
                background: yellow;
            }
        </style>
        <script language="javascript">
            function validate(){
                valid=true;
                if(document.getElementById('drafting_cost').value==""){
                    turnBlack('drc');
                }else{
                    if(isNaN(document.getElementById('drafting_cost').value)) {
                        turnRed('drc');
                        valid=false;
                    }else turnBlack('drc');
                }
                return valid;
            }
            function turnRed(field_to_change) {
                var myPara = document.getElementById(field_to_change);
                myPara.style.color = "red";
            }
            function turnBlack(field_to_change) {
                var myPara = document.getElementById(field_to_change);
                myPara.style.color = "black";
            }
            
            function sum(){
                var sm=0;
                
                var a1 = parseFloat(document.getElementById('drafting_cost').value);
                var a=0;
                if(!isNaN(a1)) a = parseFloat(a1);
                
                var b1 = parseFloat(document.getElementById('board_cost').value);
                var b=0;
                if(!isNaN(b1)) b = parseFloat(b1);
                
                var c1 = parseFloat(document.getElementById('ply_cost').value);
                var c=0;
                if(!isNaN(c1)) c = parseFloat(c1);
                
                var d1 = parseFloat(document.getElementById('cnc_cost').value);
                var d=0;
                if(!isNaN(d1)) d = parseFloat(d1);
                
                var e1 = parseFloat(document.getElementById('machining_cost').value);
                var e=0;
                if(!isNaN(e1)) e = parseFloat(e1);
                
                var f1 = parseFloat(document.getElementById('assembly_cost').value);
                var f=0;
                if(!isNaN(f1)) f = parseFloat(f1);
                
                var g1 = parseFloat(document.getElementById('disasembly_cost').value);
                var g=0;
                if(!isNaN(g1)) g = parseFloat(g1);
                
                var h1 = parseFloat(document.getElementById('paint_cost').value);
                var h=0;
                if(!isNaN(h1)) h = parseFloat(h1);
                
                var i1 = parseFloat(document.getElementById('hardware_cost').value);
                var i=0;
                if(!isNaN(i1)) i = parseFloat(i1);
                
                var j1 = parseFloat(document.getElementById('misc_cost').value);
                var j=0;
                if(!isNaN(j1)) j = parseFloat(j1);
                
                var k1 = parseFloat(document.getElementById('delivery_cost').value);
                var k=0;
                if(!isNaN(k1)) k = parseFloat(k1);
                
                var l1 = parseFloat(document.getElementById('handles_cost').value);
                var l=0;
                if(!isNaN(l1)) l = parseFloat(l1);
                
                var m1 = parseFloat(document.getElementById('measurement_cost').value);
                var m=0;
                if(!isNaN(m1)) m = parseFloat(m1);
                
                var n1 = parseFloat(document.getElementById('veneer_cost').value);
                var n=0;
                if(!isNaN(n1)) n = parseFloat(n1);
                
                var o1 = parseFloat(document.getElementById('timber_cost').value);
                var o=0;
                if(!isNaN(o1)) o = parseFloat(o1);
                
                var p1 = parseFloat(document.getElementById('bins_cost').value);
                var p=0;
                if(!isNaN(p1)) p = parseFloat(p1);
                
                var q1 = parseFloat(document.getElementById('cutlery_cost').value);
                var q=0;
                if(!isNaN(q1)) q = parseFloat(q1);
                
                var r1 = parseFloat(document.getElementById('bleach_cost').value);
                var r=0;
                if(!isNaN(r1)) r = parseFloat(r1);
                
                var s1 = parseFloat(document.getElementById('acrylic_cost').value);
                var s=0;
                if(!isNaN(s1)) s = parseFloat(s1);
                
                var t1 = parseFloat(document.getElementById('metalfin_cost').value);
                var t=0;
                if(!isNaN(t1)) t = parseFloat(t1);
                
                var u1 = parseFloat(document.getElementById('freestyle_cost').value);
                var u=0;
                if(!isNaN(u1)) u = parseFloat(u1);
                
                var v1 = parseFloat(document.getElementById('edge_cost').value);
                var v=0;
                if(!isNaN(v1)) v = parseFloat(v1);
                
                sm = a + b + c + d + e + f + g + h + i + j + k + l + m + n + o + p + q + r + s + t + u + v;
                document.getElementById('joinerytotal1').value = sm;
                document.getElementById('joinerytotal2').value = sm;
                
                a1 = parseFloat(document.getElementById('stone_cost').value);
                a=0;
                if(!isNaN(a1)) a = parseFloat(a1);
                
                b1 = parseFloat(document.getElementById('sb_cost').value);
                b=0;
                if(!isNaN(b1)) b = parseFloat(b1);
                
                c1 = parseFloat(document.getElementById('glazing_cost').value);
                c=0;
                if(!isNaN(c1)) c = parseFloat(c1);
                
                d1 = parseFloat(document.getElementById('ss_cost').value);
                d=0;
                if(!isNaN(d1)) d = parseFloat(d1);
                
                e1 = parseFloat(document.getElementById('install_cost').value);
                e=0;
                if(!isNaN(e1)) e = parseFloat(e1);
                
                f1 = parseFloat(document.getElementById('defects_cost').value);
                f=0;
                if(!isNaN(f1)) f = parseFloat(f1);
                
                var oh = calOh();
                var s2 = (sm + a + b + c + d + e + f) * (100 + oh) / 100;
                
                document.getElementById('total').value = s2;
                
                
                
            }
            
            function calOh(){
                var oh = document.getElementById('overhead').value;
                var o = parseInt(oh);
                document.getElementById('oh').innerHTML = o;
                return o;
            }
            
        </script>
    </head>
        
    <body>
    <div id='bdy'>
        <!-- make the header and the navigation -->
        <?php include_once 'includes/includeheader.php'; ?>
        <?php 
            require_once 'includes/includenav.php'; 
            generateMenu($user_type);
        ?>
        <section>
            <!-- If the user is not authorized to view the page, show a message and redirect to index -->
            <?php
            if(!in_array($user_type, $authrzd)){
                echo 'You are not authorised to see this page';
                header( "refresh:5;url=index.php" );
                exit;
            }
            
                if(time() > ($_SESSION['time'] + session_timeout)){
                    echo "Session Expired Please login again!";
                    session_destroy();
                    header( "refresh:5;url=login.php?redir=" . $path_parts['basename']);
                    exit;
                }else $_SESSION["time"] = time();
            ?>
            
            
            <?php
            
                       
            
            
            ?>
            <form id="estimateForm" name="addUser" action="estimateHandler.php" method="GET" onsubmit="return validate();" onchange="sum();">
            <article class="frm" id="orderdetail">
                <table border="0">
                <tr>
                    <td colspan="2">
                        <?php 
                        if(!$proj) {
                            echo "Order Details not found";
                        }else{
                            echo "  <tr>\n";
                            echo "    <td>\n";
                            echo "      Name :";
                            echo "    </td>\n";
                            echo "    <td>\n";
                            echo "      " . $proj->getName();
                            echo "    </td>\n";
                            echo "  </tr>\n";
                            
                            echo "  <tr>\n";
                            echo "    <td>\n";
                            echo "      Type :";
                            echo "    </td>\n";
                            echo "    <td>\n";
                            echo "      " . $proj->getProjectType();
                            echo "    </td>\n";
                            echo "  </tr>\n";
                            
                            echo "  <tr>\n";
                            echo "    <td>\n";
                            echo "      Customer Id :";
                            echo "    </td>\n";
                            echo "    <td>\n";
                            echo "      " . $proj->getCustomer()->getFirstName();
                            echo "    </td>\n";
                            echo "  </tr>\n";
                            
                            echo "  <tr>\n";
                            echo "    <td>\n";
                            echo "      Address :";
                            echo "    </td>\n";
                            echo "    <td>\n";
                            echo "      " . $proj->getProjectAddress();
                            echo "    </td>\n";
                            echo "  </tr>\n";
                            echo "</table>\n";
                        }
                        ?>
                    </td>
                </tr>
                </table>
            </article>
                
                
            <article class="frm" id="quotedetail">
                <?php
                $quote=new Quote();
                if(isset($_SESSION['quote'])){
                    $quote = $_SESSION['quote'];
                    $items = $quote->getItems();
                    if(count($items)>0){
                        echo "<table border='0'>\n";
                        echo "<tr>\n";
                        echo "<td>No.</td>\n";
                        echo "<td>Item name</td>\n";
                        echo "<td>Item Details</td>\n";
                        echo "<td>Item Price</td>\n";
                        echo "</tr>\n";
                        
                        for($i=0;$i<count($items);$i++){
                            echo "<tr>\n";
                            echo "<td>".($i+1)."</td>\n";
                            echo "<td>".$items[$i]->getItemName()."</td>\n";
                            echo "<td>".$items[$i]->getItemDetail()."</td>\n";
                            echo "<td align='right'>".$items[$i]->getItemPrice()."</td>\n";
                            echo "</tr>";
                        }
                        
                        echo "<tr>\n";
                        echo "<td></td>\n";
                        echo "<td></td>\n";
                        echo "<td></td>\n";
                        echo "<td align='right'>".$quote->getQuotePrice()."</td>\n";
                        echo "</tr>";
                        
                        echo "<tr>\n";
                        echo "<td colspan='2'>\n";
                        echo "<a class='submit' href='finalizeQuote.php' onclick=''> Finalize </a>\n";
                        echo "</td>\n";
                        echo "</tr>\n";
                        echo "</table>\n";
                    }
                }else{
                    echo "<table>\n";
                    echo "<tr>\n";
                    echo "<td colspan='2'>\n";
                    echo "Quote Details here including other items in the quote so far<br />\n";
                    echo "</td>\n";
                    echo "</tr>\n";
                    echo "</table>\n";
                }
                ?>
                
                
                    
                        
                        
                    
            </article>
                
            <article class="frm" id="basicItem">
                <table border="0">
                <tr>
                    <td class="leftcol">Item Name</td>
                    <td><input type="text" name="item_name" /></td>
                </tr>
                <tr>
                    <td>Drawings</td>
                    <td><input type="text" name="drawings"/></td>
                </tr>
                <tr>
                    <td>Details</td>
                    <td><input type="text" name="details"/></td>
                </tr>
                </table>
            </article>
            <article class="frm" id="cost">
                <table border="0">
                <tr>
                    <td class="leftcol">Drafting Cost</td>
                    <td> $ <input type="text" id="drafting_cost" class="amount" name="drafting_cost"  onkeypress="sum();"/></td>
                </tr>
                <tr>
                    <td>Board Cost</td>
                    <td> $ <input type="text" id="board_cost" class="amount" name="board_cost" /></td>
                </tr>
                <tr>
                    <td>Ply Cost</td>
                    <td> $ <input type="text" id="ply_cost" class="amount" name="ply_cost" /></td>
                </tr>
                <tr>
                    <td>CNC Cost</td>
                    <td> $ <input type="text" id="cnc_cost" class="amount" name="cnc_cost" /></td>
                </tr>
                <tr>
                    <td>Machining Cost</td>
                    <td> $ <input type="text" id="machining_cost" class="amount" name="machining_cost" /></td>
                </tr>
                <tr>
                    <td>Assembly Cost</td>
                    <td> $ <input type="text" id="assembly_cost" class="amount" name="assembly_cost" /></td>
                </tr>
                <tr>
                    <td>Disasembly Cost</td>
                    <td> $ <input type="text" id="disasembly_cost" class="amount" name="disasembly_cost" /></td>
                </tr>
                <tr>
                    <td>Paint Cost</td>
                    <td> $ <input type="text" id="paint_cost" class="amount" name="paint_cost" /></td>
                </tr>
                <tr>
                    <td>Hardware Cost</td>
                    <td> $ <input type="text" id="hardware_cost" class="amount" name="hardware_cost" /></td>
                </tr>
                <tr>
                    <td>Misc Cost</td>
                    <td> $ <input type="text" id="misc_cost" class="amount" name="misc_cost" /></td>
                </tr>
                <tr>
                    <td>Delivery Cost</td>
                    <td> $ <input type="text" id="delivery_cost" class="amount" name="delivery_cost" /></td>
                </tr>
                <tr>
                    <td>Handles Cost</td>
                    <td> $ <input type="text" id="handles_cost" class="amount" name="handles_cost" /></td>
                </tr>
                <tr>
                    <td>Measurement Cost</td>
                    <td> $ <input type="text" id="measurement_cost" class="amount" name="measurement_cost" /></td>
                </tr>
                <tr>
                    <td>Veneer Cost</td>
                    <td> $ <input type="text" id="veneer_cost" class="amount" name="veneer_cost" /></td>
                </tr>
                <tr>
                    <td>Timber Cost</td>
                    <td> $ <input type="text" id="timber_cost" class="amount" name="timber_cost" /></td>
                </tr>
                <tr>
                    <td>Bins Cost</td>
                    <td> $ <input type="text" id="bins_cost" class="amount" name="bins_cost" /></td>
                </tr>
                <tr>
                    <td>Cutlery Inserts Cost</td>
                    <td> $ <input type="text" id="cutlery_cost" class="amount" name="cutlery_cost" /></td>
                </tr>
                <tr>
                    <td>Bleach Cost</td>
                    <td> $ <input type="text" id="bleach_cost" class="amount" name="bleach_cost" /></td>
                </tr>
                <tr>
                    <td>Acrylic Cost</td>
                    <td> $ <input type="text" id="acrylic_cost" class="amount" name="acrylic_cost" /></td>
                </tr>
                <tr>
                    <td>Metal Finish Cost</td>
                    <td> $ <input type="text" id="metalfin_cost" class="amount" name="metalfin_cost" /></td>
                </tr>
                <tr>
                    <td>Freestyle Cost</td>
                    <td> $ <input type="text" id="freestyle_cost" class="amount" name="freestyle_cost" /></td>
                </tr>
                <tr>
                    <td>Edge Cost</td>
                    <td> $ <input type="text" id="edge_cost" class="amount" name="edge_cost" /></td>
                </tr>
                <tr>
                    <td>Joinery Total</td>
                    <td>$ <input type="text" class="amount" disabled="true" value="0" id="joinerytotal1" class="amount" name="joinerytotal1"/>
                    </td>
                </tr>
                </table>
            </article>
            <article class="frm"  id="door">
                <table border="0">
                <tr>
                    <td class="leftcol">Door Material</td>
                    <td><input type="radio" name="doormat" id="doormat1" value="nodoors"/><label for="doormat1">No Doors</label>
                        <input type="radio" name="doormat" id="doormat2" value="timber"/><label for="doormat2">Timber</label>
                        <input type="radio" name="doormat" id="doormat3" value="melemine"/><label for="doormat3">Malemine</label>
                        <input type="radio" name="doormat" id="doormat4" value="rawmdf"/><label for="doormat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Door Finish</td>
                    <td>
                        <input type="radio" name="doorfin" id="doorfin1" value="ucoat"/><label for="doorfin1">Undercoat</label>
                        <input type="radio" name="doorfin" id="doorfin2" value="satin"/><label for="doorfin2">Satin</label>
                        <input type="radio" name="doorfin" id="doorfin3" value="gloss"/><label for="doorfin3">Gloss</label>
                        <input type="radio" name="doorfin" id="doorfin4" value="matt"/><label for="doorfin4">Matt</label>
                    </td>
                </tr>
                <tr>
                    <td>Door Thickness</td>
                    <td>
                        <input type="radio" name="doorthick" id="doorthick1" value="16"/><label for="doorthick1">16"</label>
                        <input type="radio" name="doorthick" id="doorthick2" value="17"/><label for="doorthick2">17"</label>
                        <input type="radio" name="doorthick" id="doorthick3" value="18"/><label for="doorthick3">18"</label>
                        <input type="radio" name="doorthick" id="doorthick4" value="19"/><label for="doorthick4">19"</label>
                        <input type="radio" name="doorthick" id="doorthick5" value="25"/><label for="doorthick5">25"</label>
                        <input type="radio" name="doorthick" id="doorthick6" value="other"/><label for="doorthick6">Other</label><input type="text" name="doorthickoth"/>
                    </td>
                </tr>
                </table>
            </article>
            <article class="frm" id="benchtop">
                <table border="0">
                <tr>
                    <td class="leftcol">Benchtop Material</td>
                    <td><!--<input type="checkbox" name="benchmat" id="benchmat1" value="stone"/><label for="benchmat1">Stone</label>-->
                        <input type="checkbox" name="benchmat[]" id="benchmat2" value="solidsurf"/><label for="benchmat2">Solid Surf</label>
                        <input type="checkbox" name="benchmat[]" id="benchmat3" value="ss"/><label for="benchmat3">Stainless Steel</label>
                        <input type="checkbox" name="benchmat[]" id="benchmat4" value="laminex"/><label for="benchmat4">Laminex</label>
                        <input type="checkbox" name="benchmat[]" id="benchmat5" value="timber"/><label for="benchmat5">Timber</label>
                        <input type="checkbox" name="benchmat[]" id="benchmat6" value="veneer"/><label for="benchmat6">Veneer</label>
                        <input type="checkbox" name="benchmat[]" id="benchmat7" value="cesar"/><label for="benchmat7">Cesar Stone</label>
                    </td>
                </tr>
                <tr>
                    <td>Benchtop Material Type</td>
                    <td><input type="text" name="benchmattype"/></td>
                </tr>
                <tr>
                    <td>Benchtop Finish</td>
                    <td>
                        <input type="checkbox" name="benchfin" id="benchfin1" value="ucoat"/><label for="benchfin1">Undercoat</label>
                        <input type="checkbox" name="benchfin" id="benchfin2" value="satin"/><label for="benchfin2">Satin</label>
                        <input type="checkbox" name="benchfin" id="benchfin3" value="gloss"/><label for="benchfin3">Gloss</label>
                        <input type="checkbox" name="benchfin" id="benchfin4" value="matt"/><label for="benchfin4">Matt</label>
                    </td>
                </tr>
                <tr>
                    <td>Benchtop Thickness</td>
                    <td>
                        <input type="radio" name="benchthick" id="benchthick1" value="20"/><label for="benchthick1">20"</label>
                        <input type="radio" name="benchthick" id="benchthick2" value="30"/><label for="benchthick2">30"</label>
                        <input type="radio" name="benchthick" id="benchthick3" value="33"/><label for="benchthick3">33"</label>
                        <input type="radio" name="benchthick" id="benchthick4" value="40"/><label for="benchthick4">40"</label>
                        <input type="radio" name="benchthick" id="benchthick5" value="60"/><label for="benchthick5">60"</label>
                        <input type="radio" name="benchthick" id="benchthick6" value="other"/><label for="benchthick6">Other</label><input type="text" name="doorthickoth"/>
                    </td>
                </tr>
                </table>
            </article>
            <article class="frm"  id="kickboard">
                <table border="0">
                <tr>
                    <td class="leftcol">Kickboard Material</td>
                    <td><input type="checkbox" name="kbmat" id="kbmat1" value="nodoors"/><label for="kbmat1">No Doors</label>
                        <input type="checkbox" name="kbmat" id="kbmat2" value="timber"/><label for="kbmat2">Timber</label>
                        <input type="checkbox" name="kbmat" id="kbmat3" value="melemine"/><label for="kbmat3">Malemine</label>
                        <input type="checkbox" name="kbmat" id="kbmat4" value="rawmdf"/><label for="kbmat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Kickboard Finish</td>
                    <td>
                        <input type="checkbox" name="kbfin" id="kbfin1" value="ucoat"/><label for="kbfin1">Undercoat</label>
                        <input type="checkbox" name="kbfin" id="kbfin2" value="satin"/><label for="kbfin2">Satin</label>
                        <input type="checkbox" name="kbfin" id="kbfin3" value="gloss"/><label for="kbfin3">Gloss</label>
                        <input type="checkbox" name="kbfin" id="kbfin4" value="matt"/><label for="kbfin4">Matt</label>
                    </td>
                </tr>
                <tr>
                    <td>Kickboard Thickness</td>
                    <td>
                        <input type="radio" name="kbthick" id="kbthick1" value="16"/><label for="kbthick1">16"</label>
                        <input type="radio" name="kbthick" id="kbthick2" value="17"/><label for="kbthick2">17"</label>
                        <input type="radio" name="kbthick" id="kbthick3" value="18"/><label for="kbthick3">18"</label>
                        <input type="radio" name="kbthick" id="kbthick4" value="19"/><label for="kbthick4">19"</label>
                        <input type="radio" name="kbthick" id="kbthick5" value="25"/><label for="kbthick5">25"</label>
                        <input type="radio" name="kbthick" id="kbthick6" value="other"/><label for="kbthick6">Other</label><input type="text" name="doorthickoth"/>
                    </td>
                </tr>
                </table>
            </article>
            <article class="frm" id="baulkhead">
                <table border="0">
                <tr>
                    <td class="leftcol">Baulkhead Material</td>
                    <td><input type="checkbox" name="bhmat" id="bhmat1" value="nodoors"/><label for="bhmat1">No Doors</label>
                        <input type="checkbox" name="bhmat" id="bhmat2" value="timber"/><label for="bhmat2">Timber</label>
                        <input type="checkbox" name="bhmat" id="bhmat3" value="melemine"/><label for="bhmat3">Malemine</label>
                        <input type="checkbox" name="bhmat" id="bhmat4" value="rawmdf"/><label for="bhmat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Baulkhead Finish</td>
                    <td>
                        <input type="checkbox" name="bhfin" id="bhfin1" value="ucoat"/><label for="bhfin1">Undercoat</label>
                        <input type="checkbox" name="bhfin" id="bhfin2" value="satin"/><label for="bhfin2">Satin</label>
                        <input type="checkbox" name="bhfin" id="bhfin3" value="gloss"/><label for="bhfin3">Gloss</label>
                        <input type="checkbox" name="bhfin" id="bhfin4" value="matt"/><label for="bhfin4">Matt</label>
                    </td>
                </tr>
                </tr>
                </table>
            </article>
            <article class="frm" id="handles">
                <table border="0">
                <tr>
                    <td class="leftcol">Handle Type</td>
                    <td><input type="text" name="handle_type"/></td>
                </tr>
                </table>
            </article>
            <article class="frm"  id="drawers">
                <table border="0">
                <tr>
                    <td class="leftcol">Drawer Type</td>
                    <td><input type="radio" name="drawermat" id="drawermat1" value="nodrawers"/><label for="drawermat1">No drawers</label>
                        <input type="radio" name="drawermat" id="drawermat2" value="timber"/><label for="drawermat2">Timber</label>
                        <input type="radio" name="drawermat" id="drawermat3" value="melemine"/><label for="drawermat3">Malemine</label>
                        <input type="radio" name="drawermat" id="drawermat4" value="rawmdf"/><label for="drawermat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Drawer Capacity</td>
                    <td>
                        <input type="radio" name="drawercap" id="drawercap1" value="30"/><label for="drawercap1">30kg</label>
                        <input type="radio" name="drawercap" id="drawercap2" value="65"/><label for="drawercap2">65kg</label>
                        <input type="radio" name="drawercap" id="drawercap3" value="80"/><label for="drawercap3">80kg</label>
                    </td>
                </tr>
                </table>
            </article>
            <article class="frm"  id="ss">
                <table border="0">
                <tr>
                    <td class="leftcol">Stainless Steel Components</td>
                    <td><input type="text" name="ss" id="ss"/></td>
                </tr>
                </table>
            </article>   
            <article class="frm"  id="splashback">
                <table border="0">
                <tr>
                    <td class="leftcol">Splashback Material</td>
                    <td><input type="checkbox" name="sbmat" id="sbmat1" value="nodoors"/><label for="sbmat1">No Doors</label>
                        <input type="checkbox" name="sbmat" id="sbmat2" value="timber"/><label for="sbmat2">Timber</label>
                        <input type="checkbox" name="sbmat" id="sbmat3" value="melemine"/><label for="sbmat3">Malemine</label>
                        <input type="checkbox" name="sbmat" id="sbmat4" value="rawmdf"/><label for="sbmat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Splashback Finish</td>
                    <td>
                        <input type="checkbox" name="sbfin" id="sbfin1" value="ucoat"/><label for="sbfin1">Undercoat</label>
                        <input type="checkbox" name="sbfin" id="sbfin2" value="satin"/><label for="sbfin2">Satin</label>
                        <input type="checkbox" name="sbfin" id="sbfin3" value="gloss"/><label for="sbfin3">Gloss</label>
                        <input type="checkbox" name="sbfin" id="sbfin4" value="matt"/><label for="sbfin4">Matt</label>
                    </td>
                </tr>
                <tr>
                    <td>Splashback Thickness</td>
                    <td>
                        <input type="radio" name="sbthick" id="sbthick1" value="16"/><label for="sbthick1">16"</label>
                        <input type="radio" name="sbthick" id="sbthick2" value="17"/><label for="sbthick2">17"</label>
                        <input type="radio" name="sbthick" id="sbthick3" value="18"/><label for="sbthick3">18"</label>
                        <input type="radio" name="sbthick" id="sbthick4" value="19"/><label for="sbthick4">19"</label>
                        <input type="radio" name="sbthick" id="sbthick5" value="25"/><label for="sbthick5">25"</label>
                        <input type="radio" name="sbthick" id="sbthick6" value="other"/><label for="sbthick6">Other</label><input type="text" name="doorthickoth"/>
                    </td>
                </tr>
                </table>
            </article> 
            <article class="frm"  id="hardware">
                <table border="0">
                <tr>
                    <td class="leftcol">Hardwares</td>
                    <td><input type="checkbox" name="hardware" id="hardware1" value="nodoors"/><label for="hardware1">No Doors</label>
                        <input type="checkbox" name="hardware" id="hardware2" value="timber"/><label for="hardware2">Timber</label>
                        <input type="checkbox" name="hardware" id="hardware3" value="melemine"/><label for="hardware3">Malemine</label>
                        <input type="checkbox" name="hardware" id="hardware4" value="rawmdf"/><label for="hardware4">Raw MDF</label>
                    </td>
                </tr>
                </table>
            </article>   
            <article class="frm"  id="carcass">
                <table border="0">
                <tr>
                    <td class="leftcol">Carcass Material</td>
                    <td><input type="checkbox" name="carcassmat" id="carcassmat1" value="nodoors"/><label for="carcassmat1">No Doors</label>
                        <input type="checkbox" name="carcassmat" id="carcassmat2" value="timber"/><label for="carcassmat2">Timber</label>
                        <input type="checkbox" name="carcassmat" id="carcassmat3" value="melemine"/><label for="carcassmat3">Malemine</label>
                        <input type="checkbox" name="carcassmat" id="carcassmat4" value="rawmdf"/><label for="carcassmat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Carcass Finish</td>
                    <td>
                        <input type="checkbox" name="carcassfin" id="carcassfin1" value="ucoat"/><label for="carcassfin1">Undercoat</label>
                        <input type="checkbox" name="carcassfin" id="carcassfin2" value="satin"/><label for="carcassfin2">Satin</label>
                        <input type="checkbox" name="carcassfin" id="carcassfin3" value="gloss"/><label for="carcassfin3">Gloss</label>
                        <input type="checkbox" name="carcassfin" id="carcassfin4" value="matt"/><label for="carcassfin4">Matt</label>
                    </td>
                </tr>
                <tr>
                    <td>Carcass Thickness</td>
                    <td>
                        <input type="radio" name="carcassthick" id="carcassthick1" value="16"/><label for="carcassthick1">16"</label>
                        <input type="radio" name="carcassthick" id="carcassthick2" value="17"/><label for="carcassthick2">17"</label>
                        <input type="radio" name="carcassthick" id="carcassthick3" value="18"/><label for="carcassthick3">18"</label>
                        <input type="radio" name="carcassthick" id="carcassthick4" value="19"/><label for="carcassthick4">19"</label>
                        <input type="radio" name="carcassthick" id="carcassthick5" value="25"/><label for="carcassthick5">25"</label>
                        <input type="radio" name="carcassthick" id="carcassthick6" value="other"/><label for="carcassthick6">Other</label><input type="text" name="doorthickoth"/>
                    </td>
                </tr>
                <tr>
                    <td>Carcass Edge Material</td>
                    <td><input type="checkbox" name="carcassedgemat" id="carcassedgemat1" value="nodoors"/><label for="carcassedgemat1">No Doors</label>
                        <input type="checkbox" name="carcassedgemat" id="carcassedgemat2" value="timber"/><label for="carcassedgemat2">Timber</label>
                        <input type="checkbox" name="carcassedgemat" id="carcassedgemat3" value="melemine"/><label for="carcassedgemat3">Malemine</label>
                        <input type="checkbox" name="carcassedgemat" id="carcassedgemat4" value="rawmdf"/><label for="carcassedgemat4">Raw MDF</label>
                    </td>
                </tr>
                <tr>
                    <td>Carcass Edge Thickness</td>
                    <td>
                        <input type="radio" name="carcassedgethick" id="carcassedgethick1" value="16"/><label for="carcassedgethick1">16"</label>
                        <input type="radio" name="carcassedgethick" id="carcassedgethick2" value="17"/><label for="carcassedgethick2">17"</label>
                        <input type="radio" name="carcassedgethick" id="carcassedgethick3" value="18"/><label for="carcassedgethick3">18"</label>
                        <input type="radio" name="carcassedgethick" id="carcassedgethick4" value="19"/><label for="carcassedgethick4">19"</label>
                        <input type="radio" name="carcassedgethick" id="carcassedgethick5" value="25"/><label for="carcassedgethick5">25"</label>
                        <input type="radio" name="carcassedgethick" id="carcassedgethick6" value="other"/><label for="carcassedgethick6">Other</label><input type="text" name="doorthickoth"/>
                    </td>
                </tr>                
                </table>
            </article> 
            <article class="frm"  id="totals">
                <table border="0">
                <tr>
                    <td class="leftcol">Joinery Total</td>
                    <td>$ <input type="text" class="amount" disabled="true" id="joinerytotal2" name="joinerytotal2" value="0"/>
                    </td>
                </tr>
                <tr>
                    <td>Stone/Solid Materials</td>
                    <td> $ <input type="text" id="stone_cost" class="amount" name="stone_cost" /></td>
                </tr>
                <tr>
                    <td>Splashback Cost</td>
                    <td> $ <input type="text" id="sb_cost" class="amount" name="sb_cost" /></td>
                </tr>
                <tr>
                    <td>Glazing Cost</td>
                    <td> $ <input type="text" id="glazing_cost" class="amount" name="glazing_cost" /></td>
                </tr>
                <tr>
                    <td id="ssc">Stainless Steel Cost</td>
                    <td> $ <input type="text" id="ss_cost" class="amount" name="ss_cost" /></td>
                </tr>
                <tr>
                    <td>Installation Cost</td>
                    <td> $ <input type="text" id="install_cost" class="amount" name="install_cost" /></td>
                </tr>
                <tr>
                    <td>Defects</td>
                    <td> $ <input type="text" id="defects_cost" class="amount" name="defects_cost" /></td>
                </tr>
                <tr>
                    <td>Overhead %</td>
                    <td>&nbsp;&nbsp;&nbsp;<input type="range" id="overhead" class="amount" name="overhead" min="10" max="45" value="25" onchange="calOh();" style="width:170px;" /> <div style="display:inline;" id="oh" name="oh">25</div>%</td>
                </tr>
                </table>
            </article>   
                
                
            <article class="frm"  id="finaltotals">
                <table border="0">
                <tr>
                    <td width='200px'>Total</td>
                    <td> $ <input type="text" class="amount" disabled="true" id="total" value="0" name="total" style="display:inline;"/></td>
                </tr>
                </table>
            </article>   
                
                <a class="submit" href="#" onclick="document.getElementById('estimateForm').submit();"> Save </a>
            </form>
        </section>
        <?php include_once 'includes/includefooter.html'; ?>
    </div>
    </body>
    
</html>