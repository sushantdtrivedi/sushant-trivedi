<?php
// put all the files that are required at the top
require_once './classes/User.php';
require_once './classes/Job.php';
require_once './classes/epk_db_connect.php';
require_once './classes/utils.php';
require_once './classes/Quote.php';
require_once './classes/Item.php';

// if there is nobody logged in, redirect to index.php
session_start();
if(!isset($_SESSION['usr'])){
    header('location: login.php?redir=index.php');
    exit;
}

// specify the authorised users
$authrzd = array('techie', 'estimator', 'owner'); // 'techie', 'admin', 'cutter', 'customer', 'delivery', 'drafter', 'estimator', 'installer', 'painter', 'cabinate maker', 'painter', 'packer', 'owner''

// get the details of the logged in user
$usr = $_SESSION['usr'];
$user_type=$usr->getRole();


?>
<?php
            if(!in_array($user_type, $authrzd)){
                echo 'You are not authorised to see this page';
                header( "refresh:5;url=index.php" );
                exit;
            }
            
            if(time() > ($_SESSION['time'] + session_timeout)){
                echo "Session Expired Please login again!";
                session_destroy();
                header( "refresh:5;url=login.php?redir=" . $path_parts['basename']);
                exit;
            }else $_SESSION["time"] = time();
            
            $quote = new Quote();
            
            if(isset($_SESSION['quote'])){
                $quote = $_SESSION['quote'];
            }
            
            
            
            $item_name = $_REQUEST['item_name'];
            $drawings = $_REQUEST['drawings'];
            $details = $_REQUEST['details'];
            
            $drafting_cost = floatval($_REQUEST['drafting_cost']);
            $board_cost = floatval($_REQUEST['board_cost']);
            $ply_cost = floatval($_REQUEST['ply_cost']);
            $cnc_cost = floatval($_REQUEST['cnc_cost']);
            $machining_cost = floatval($_REQUEST['machining_cost']);
            $assembly_cost = floatval($_REQUEST['assembly_cost']);
            $disasembly_cost = floatval($_REQUEST['disasembly_cost']);
            $paint_cost = floatval($_REQUEST['paint_cost']);
            $hardware_cost = floatval($_REQUEST['hardware_cost']);
            $misc_cost = floatval($_REQUEST['misc_cost']);
            $delivery_cost = floatval($_REQUEST['delivery_cost']);
            $handles_cost = floatval($_REQUEST['handles_cost']);
            $measurement_cost = floatval($_REQUEST['measurement_cost']);
            $veneer_cost = floatval($_REQUEST['veneer_cost']);
            $timber_cost = floatval($_REQUEST['timber_cost']);
            $bins_cost = floatval($_REQUEST['bins_cost']);
            $cutlery_cost = floatval($_REQUEST['cutlery_cost']);
            $bleach_cost = floatval($_REQUEST['bleach_cost']);
            $acrylic_cost = floatval($_REQUEST['acrylic_cost']);
            $metalfin_cost = floatval($_REQUEST['metalfin_cost']);
            $freestyle_cost = floatval($_REQUEST['freestyle_cost']);
            $edge_cost = floatval($_REQUEST['edge_cost']);
            
            $joinerytotal = $drafting_cost + $board_cost + $ply_cost + $cnc_cost
                    + $machining_cost + $assembly_cost + $disasembly_cost
                    + $paint_cost + $hardware_cost + $misc_cost + $delivery_cost
                    + $handles_cost + $measurement_cost + $veneer_cost 
                    + $timber_cost + $bins_cost + $cutlery_cost + $bleach_cost
                    + $acrylic_cost + $metalfin_cost + $freestyle_cost
                    + $edge_cost;
            
            $doormat = isset($_REQUEST['doormat']) ? $_REQUEST['doormat'] : "None"; // door material
            $doorfin = isset($_REQUEST['doorfin']) ? $_REQUEST['doorfin'] : "None"; // door finish
            $doorthick = isset($_REQUEST['doorthick']) ? $_REQUEST['doorthick'] : "None"; // door thickness
            
            $benchmat = isset($_REQUEST['benchmat']) ? $_REQUEST['benchmat'] : "None"; // benchtop material
            $benchfin = isset($_REQUEST['benchfin']) ? $_REQUEST['benchfin'] : "None"; // benchtop finish
            $benchthick = isset($_REQUEST['benchthick']) ? $_REQUEST['benchthick'] : "None"; // benchtop thickness
            
            $kbmat= isset($_REQUEST['kbmat']) ? $_REQUEST['kbmat'] : "None"; // kickboard material
            $kbfin= isset($_REQUEST['kbfin']) ? $_REQUEST['kbfin'] : "None"; // kickboard material
            $kbthick = isset($_REQUEST['kbthick']) ? $_REQUEST['kbthick'] : "None"; // kickboard thickness
            
            $bhmat= isset($_REQUEST['bhmat']) ? $_REQUEST['bhmat'] : "None"; // bulkhead material
            $bhfin= isset($_REQUEST['bhfin']) ? $_REQUEST['bhfin'] : "None"; // bulkhead material
            
            $handle_type= $_REQUEST['handle_type']; // handle type to use
            
            $drawermat= isset($_REQUEST['drawermat']) ? $_REQUEST['drawermat'] : "None"; // material of the drawer
            $drawercap= isset($_REQUEST['drawercap']) ? $_REQUEST['drawercap'] : "None"; // capacity of the drawer
            
            $ss = $_REQUEST['ss']; //stainless steel component
            
            $sbmat = isset($_REQUEST['sbmat']) ? $_REQUEST['sbmat'] : "None"; //splashback material
            $sbfin = isset($_REQUEST['sbfin']) ? $_REQUEST['sbfin'] : "None"; //splashback finish
            $sbthick = isset($_REQUEST['sbthick']) ? $_REQUEST['sbthick'] : "None"; //splashback thickness
            
            $hardware = isset($_REQUEST['hardware']) ? $_REQUEST['hardware'] : "None"; //Hardwares
            
            $carcassmat = isset($_REQUEST['carcassmat']) ? $_REQUEST['carcassmat'] : "None"; //Carcass Material
            $carcassfin = isset($_REQUEST['carcassfin']) ? $_REQUEST['carcassfin'] : "None"; //Carcass Finish
            $carcassthick = isset($_REQUEST['carcassthick']) ? $_REQUEST['carcassthick'] : "None"; //Carcass Thickness
            $carcassedgemat = isset($_REQUEST['carcassedgemat']) ? $_REQUEST['carcassedgemat'] : "None"; //Carcass Edge Material
            $carcassedgethick = isset($_REQUEST['carcassedgethick']) ? $_REQUEST['carcassedgethick'] : "None"; //Carcass Edge Thickness
            
            $stone_cost = $_REQUEST['stone_cost']; //Cost of Stone Materials
            $sb_cost = $_REQUEST['sb_cost']; //Cost of Splashback
            $glazing_cost = $_REQUEST['glazing_cost']; //Glazing Cost
            $ss_cost = $_REQUEST['ss_cost']; //Stainless Steel Cost
            $install_cost = $_REQUEST['install_cost']; //Installation Cost
            $defects_cost = $_REQUEST['defects_cost']; //Defect costs
            $overhead = $_REQUEST['overhead']; // Overhead percentage
            
            $materialtotal = $stone_cost + $sb_cost + $glazing_cost
                    + $ss_cost + $install_cost + $defects_cost;
            
            $subtotal = $joinerytotal + $materialtotal;
            $oh = $subtotal * $overhead / 100;
            $gst = ($subtotal + $oh) * 0.1;
            $finalcost = $subtotal + $oh + $gst;
            
            
            if(isset($_SESSION['quote'])) $quote = $_SESSION['quote'];
            $item=new Item();
            $item->setItemName($item_name);
            $item->setItemDetail($details);
            $item->setItemDrawings($drawings);
    
            $item->setDraftingCost($drafting_cost);
            $item->setBoardCost($board_cost);
            $item->setPlyCost($ply_cost);
            $item->setCNCCost($cnc_cost);
            $item->setMachiningCost($machining_cost);
            $item->setAssemblyCost($assembly_cost);
            $item->setDisasemblyCost($disasembly_cost);
            $item->setPaintCost($paint_cost);
            $item->setHardwareCost($hardware_cost);
            $item->setMiscCost($misc_cost);
            $item->setDeliveryCost($delivery_cost);
            $item->setHandlesCost($handles_cost);
            $item->setMeasurementCost($measurement_cost);
            $item->setVeneerCost($veneer_cost);
            $item->setTimberCost($timber_cost);
            $item->setBinsCost($bins_cost);
            $item->setCutleryCost($cutlery_cost);
            $item->setBleachCost($bleach_cost);
            $item->setAcrylicCost($acrylic_cost);
            $item->setMetalFinishCost($metalfin_cost);
            $item->setFreestyleCost($freestyle_cost);
            $item->setEdgeCost($edge_cost);

            $item->setDoorMaterial($doormat);
            $item->setDoorFinish($doorfin);
            $item->setDoorThickness($doorthick);
            
            $item->setBenchMaterial($benchmat);
            $item->setBenchFinish($benchfin);
            $item->setBenchThickness($benchthick);

            $item->setKickboardMaterial($kbmat);
            $item->setKickboardFinish($kbfin);
            $item->setKickboardThickness($kbthick);

            $item->setBulkheadMaterial($bhmat);
            $item->setBulkheadFinish($bhfin);

            $item->setHandles($handle_type);

            $item->setDrawerMaterial($drawermat);
            $item->setDrawerCapacity($drawercap);

            $item->setSteelComponents($ss);

            $item->setSplashbackMaterial($sbmat);
            $item->setSplashbackFinish($sbfin);
            $item->setSplashbackThickness($sbthick);

            $item->setHardwares($hardware);

            $item->setCarcassMaterial($carcassmat);
            $item->setCarcassFinish($carcassfin);
            $item->setCarcassThickness($carcassthick);
            $item->setCarcassEdgeMaterial($carcassedgemat);
            $item->setCarcassEdgeThickness($carcassedgethick);

            $item->setStoneCost($stone_cost);
            $item->setSplashbackCost($sb_cost);
            $item->setGlazingCost($glazing_cost);
            $item->setStainlessSteelCost($ss_cost);
            $item->setInstallationCost($install_cost);
            $item->setDefectsCost($defects_cost);

            $item->setOverheadPercentage($overhead);
            
            $quote->addItem($item);
            
            $_SESSION['quote'] = $quote;
            header('location:estimate.php?id=1');
            
            
            
            ?>
        </section>
        <?php include_once 'includes/includefooter.html'; ?>
    </div>
    </body>
    
</html>